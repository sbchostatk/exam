package com.java.exam.api;

import com.java.exam.dto.StudentDTO;
import com.java.exam.model.StudentEntity;

import java.util.List;

public interface StudentService {
    StudentDTO getStudentById(int id);

    StudentDTO create(StudentDTO studentDTO);

    List<StudentDTO> getAll();

    StudentDTO delete(int id);

    StudentDTO update(StudentDTO studentDTO);

}
