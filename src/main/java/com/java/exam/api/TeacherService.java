package com.java.exam.api;

import com.java.exam.dto.StudentDTO;
import com.java.exam.dto.TeacherDTO;

import java.util.List;

public interface TeacherService {
    TeacherDTO getTeacherById(int id);

    TeacherDTO create(TeacherDTO teacherDTO);

    List<TeacherDTO> getAll();

    TeacherDTO delete(int id);

    TeacherDTO update(TeacherDTO teacherDTO);
}
