package com.java.exam.api;

import com.java.exam.dto.StudentDTO;
import com.java.exam.dto.TeacherDTO;
import com.java.exam.model.TeacherStudentEntity;

import java.util.List;

public interface TeacherStudentService {
    void create(TeacherStudentEntity teacherStudent);

    List<StudentDTO> getStudents(TeacherDTO teacher);

    List<TeacherDTO> getTeachers(StudentDTO student);

    boolean delete(TeacherDTO teacher, StudentDTO student);

}
