package com.java.exam.controller;

import com.java.exam.api.StudentService;
import com.java.exam.dto.StudentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/students")
public class StudentController {
    @Autowired
    private StudentService studentService;

    //добавить студента
    @PostMapping(value = "/add")
    public StudentDTO addStudent(@RequestBody StudentDTO student) {
        return studentService.create(student);
    }

    //удалить студента
    @DeleteMapping(value = "/delete/{id}")
    public StudentDTO deleteStudent(@PathVariable(name = "id") int id) {
        return studentService.delete(id);
    }

    //получить одного студента
    @GetMapping(value = "/getStudent/{id}")
    public StudentDTO getStudent(@PathVariable(name = "id") int id) {
        return studentService.getStudentById(id);
    }

    //получить всех студентов
    @GetMapping("/getAll")
    public List<StudentDTO> getAllStudents() {
        return studentService.getAll();
    }

    //получить всех студентов
    @PutMapping("/update")
    public StudentDTO updateStudent(@RequestBody StudentDTO student) {
        return studentService.update(student);
    }

}
