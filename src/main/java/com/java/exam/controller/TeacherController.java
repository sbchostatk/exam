package com.java.exam.controller;

import com.java.exam.api.StudentService;
import com.java.exam.api.TeacherService;
import com.java.exam.dto.StudentDTO;
import com.java.exam.dto.TeacherDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/teachers")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    //добавить студента
    @PostMapping(value = "/add")
    public TeacherDTO addStudent(@RequestBody TeacherDTO teacherDTO) {
        return teacherService.create(teacherDTO);
    }

    //удалить студента
    @DeleteMapping(value = "/delete/{id}")
    public TeacherDTO deleteStudent(@PathVariable(name = "id") int id) {
        return teacherService.delete(id);
    }

    //получить одного студента
    @GetMapping(value = "/getTeacher/{id}")
    public TeacherDTO getStudent(@PathVariable(name = "id") int id) {
        return teacherService.getTeacherById(id);
    }

    //получить всех студентов
    @GetMapping("/getAll")
    public List<TeacherDTO> getAllStudents() {
        return teacherService.getAll();
    }

    //получить всех студентов
    @PutMapping("/update")
    public TeacherDTO updateStudent(@RequestBody TeacherDTO teacherDTO) {
        return teacherService.update(teacherDTO);
    }
}
