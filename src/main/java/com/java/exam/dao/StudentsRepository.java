package com.java.exam.dao;

import com.java.exam.model.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentsRepository extends JpaRepository<StudentEntity, Integer> {
}
