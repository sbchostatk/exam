package com.java.exam.dao;

import com.java.exam.model.StudentEntity;
import com.java.exam.model.TeacherEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<TeacherEntity, Integer> {
}
