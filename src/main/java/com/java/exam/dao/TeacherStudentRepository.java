package com.java.exam.dao;

import com.java.exam.dto.StudentDTO;
import com.java.exam.dto.TeacherDTO;
import com.java.exam.model.StudentEntity;
import com.java.exam.model.TeacherEntity;
import com.java.exam.model.TeacherStudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeacherStudentRepository extends JpaRepository<TeacherStudentEntity, Integer> {
    TeacherStudentEntity findByTeacherAndStudent(TeacherDTO teacher, StudentDTO student);
    List<TeacherStudentEntity> findAllByTeacher(TeacherDTO teacher);

    List<TeacherStudentEntity> findAllByStudent(StudentDTO student);

}
