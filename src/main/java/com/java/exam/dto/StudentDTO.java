package com.java.exam.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDTO {
    private int student_id;
    private String name;
    private String middle_name;
    private String last_name;
    private String specialty;
    private int course;
}
