package com.java.exam.dto;

import com.java.exam.model.TeacherStudentEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeacherDTO {
    private int teacher_id;
    private String name;
    private String middle_name;
    private String last_name;
    private String faculty;
}
