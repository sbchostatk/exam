package com.java.exam.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "Student")
public class StudentEntity {
    @Id
    @Column(name = "student_id")
    @SequenceGenerator(name = "studentIdSeq", sequenceName = "student_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "studentIdSeq")
    private int student_id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String middle_name;

    @Column(nullable = false)
    private String last_name;

    @Column
    private String specialty;

    @Column
    private int course;

    public void setName(String name) {
        this.name = name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "student")
    private List<TeacherStudentEntity> teacherStudentList;

    protected StudentEntity() {}

    public StudentEntity(String name, String middle_name, String last_name, String specialty, int course, List<TeacherStudentEntity> teacherStudentList) {
        super();
        this.name = name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.specialty = specialty;
        this.course = course;
        this.teacherStudentList = teacherStudentList;
    }

    public int getId() {
        return student_id;
    }

    public String getName() {
        return name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public int getCourse() {
        return course;
    }

    public String getSpecialty() {
        return specialty;
    }

    /*@Override
    public String toString() {
        return getName() + " " + getMiddle_name() + " " + getLast_name() +
                ", " + getCourse() + " курс";
    }*/

}
