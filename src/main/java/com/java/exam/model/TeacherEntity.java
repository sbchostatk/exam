package com.java.exam.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "Teacher")
public class TeacherEntity {

    @Id
    @Column(name = "teacher_id")
    @SequenceGenerator(name = "teacherIdSeq", sequenceName = "teacher_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "teacherIdSeq")
    private int teacher_id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String middle_name;

    @Column(nullable = false)
    private String last_name;

    @Column
    private String faculty;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher")
    private List<TeacherStudentEntity> teacherStudentList;

    protected TeacherEntity() {
    }

    public TeacherEntity(String name, String middle_name, String last_name, String subject) {
        super();
        this.name = name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.faculty = subject;
        teacherStudentList = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    public int getId() {
        return teacher_id;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getFaculty() {
        return faculty;
    }

    /*@Override
    public String toString() {
        return getName() + " " + getMiddle_name() + " " + getLast_name() +
                ", предмет: " + getSubject();
    }*/

}
