package com.java.exam.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "Teacher_Student")
public class TeacherStudentEntity {
    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "teacherStudentIdSeq", sequenceName = "teacher_student_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "teacherStudentIdSeq")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "teacher_id", nullable = false)
    private TeacherEntity teacher;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id", nullable = false)
    private StudentEntity student;


    public TeacherStudentEntity() {

    }

    public TeacherStudentEntity(TeacherEntity teacher, StudentEntity student) {
        this.teacher = teacher;
        this.student = student;
    }

    public int getId() {
        return id;
    }

    @JsonIgnore
    public TeacherEntity getTeacher() {
        return teacher;
    }

    @JsonIgnore
    public StudentEntity getStudent() {
        return student;
    }

}
