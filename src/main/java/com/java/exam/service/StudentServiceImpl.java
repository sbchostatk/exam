package com.java.exam.service;

import com.java.exam.api.StudentService;
import com.java.exam.dao.StudentsRepository;
import com.java.exam.dto.StudentDTO;
import com.java.exam.model.StudentEntity;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class StudentServiceImpl implements StudentService {
    private final StudentsRepository studentsRepository;
    private final ModelMapper modelMapper;

    public StudentServiceImpl(StudentsRepository studentsRepository, ModelMapper modelMapper) {
        this.studentsRepository = studentsRepository;
        this.modelMapper = modelMapper;
        this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    @Override
    public StudentDTO getStudentById(int id) {
        StudentEntity studentEntity = studentsRepository.findById(id).get();
        log.debug("Получена сущность с id={}", id);
        return modelMapper.map(studentEntity, StudentDTO.class);
    }

    @Override
    public StudentDTO create(StudentDTO studentDTO) {
        StudentEntity studentEntity = modelMapper.map(studentDTO, StudentEntity.class);
        studentsRepository.save(studentEntity);
        return getStudentById(studentEntity.getId());
    }

    @Override
    public List<StudentDTO> getAll() {
        List<StudentEntity> entities = studentsRepository.findAll();
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public StudentDTO delete(int id) {
        if (studentsRepository.existsById(id)) {
            StudentEntity studentEntity = studentsRepository.findById(id).get();
            studentsRepository.deleteById(id);
            return modelMapper.map(studentEntity, StudentDTO.class);
        }
        return new StudentDTO();
    }

    @Override
    public StudentDTO update(StudentDTO studentDTO) {
        StudentEntity studentEntity = modelMapper.map(studentDTO, StudentEntity.class);
        StudentEntity oldStudent = studentsRepository.findById(studentEntity.getId()).get();
        oldStudent.setName(studentDTO.getName());
        oldStudent.setLast_name(studentDTO.getLast_name());
        oldStudent.setMiddle_name(studentDTO.getMiddle_name());
        oldStudent.setCourse(studentDTO.getCourse());
        oldStudent.setSpecialty(studentDTO.getSpecialty());
        studentsRepository.save(oldStudent);
        return getStudentById(oldStudent.getId());
    }
}
