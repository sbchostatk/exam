package com.java.exam.service;

import com.java.exam.api.TeacherService;
import com.java.exam.dao.StudentsRepository;
import com.java.exam.dao.TeacherRepository;
import com.java.exam.dto.StudentDTO;
import com.java.exam.dto.TeacherDTO;
import com.java.exam.model.StudentEntity;
import com.java.exam.model.TeacherEntity;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class TeacherServiceImpl implements TeacherService {

    private final TeacherRepository teacherRepository;
    private final ModelMapper modelMapper;

    public TeacherServiceImpl(TeacherRepository teacherRepository, ModelMapper modelMapper) {
        this.teacherRepository = teacherRepository;
        this.modelMapper = modelMapper;
        this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STANDARD);
    }

    @Override
    public TeacherDTO getTeacherById(int id) {
        TeacherEntity teacherEntity = teacherRepository.findById(id).get();
        log.debug("Получена сущность с id={}", id);
        return modelMapper.map(teacherEntity, TeacherDTO.class);
    }

    @Override
    public TeacherDTO create(TeacherDTO teacherDTO) {
        TeacherEntity teacherEntity = modelMapper.map(teacherDTO, TeacherEntity.class);
        teacherRepository.save(teacherEntity);
        return getTeacherById(teacherEntity.getId());
    }

    @Override
    public List<TeacherDTO> getAll() {
        List<TeacherEntity> entities = teacherRepository.findAll();
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public TeacherDTO delete(int id) {
        if (teacherRepository.existsById(id)) {
            TeacherEntity teacherEntity = teacherRepository.findById(id).get();
            teacherRepository.deleteById(id);
            return modelMapper.map(teacherEntity, TeacherDTO.class);
        }
        return new TeacherDTO();
    }

    @Override
    public TeacherDTO update(TeacherDTO teacherDTO) {
        TeacherEntity teacherEntity = modelMapper.map(teacherDTO, TeacherEntity.class);
        TeacherEntity oldTeacher = teacherRepository.findById(teacherEntity.getId()).get();
        oldTeacher.setName(teacherDTO.getName());
        oldTeacher.setLast_name(teacherDTO.getLast_name());
        oldTeacher.setMiddle_name(teacherDTO.getMiddle_name());
        oldTeacher.setFaculty(teacherDTO.getFaculty());
        teacherRepository.save(oldTeacher);
        return getTeacherById(oldTeacher.getId());
    }
}
