package com.java.exam.service;

import com.java.exam.api.TeacherStudentService;
import com.java.exam.dao.StudentsRepository;
import com.java.exam.dao.TeacherRepository;
import com.java.exam.dao.TeacherStudentRepository;
import com.java.exam.dto.StudentDTO;
import com.java.exam.dto.TeacherDTO;
import com.java.exam.model.TeacherStudentEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeacherStudentServiceImpl implements TeacherStudentService {
    @Autowired
    private final TeacherStudentRepository teacherStudentRepository;

    @Autowired
    private final StudentsRepository studentRepository;

    @Autowired
    private final TeacherRepository teacherRepository;

    public TeacherStudentServiceImpl(TeacherStudentRepository teacherStudentRepository, StudentsRepository studentRepository, TeacherRepository teacherRepository) {
        this.teacherStudentRepository = teacherStudentRepository;
        this.studentRepository = studentRepository;
        this.teacherRepository = teacherRepository;
    }

    @Override
    public void create(TeacherStudentEntity teacherStudent) {
        teacherStudentRepository.save(teacherStudent);
    }

    @Override
    public List<StudentDTO> getStudents(TeacherDTO teacher) {
        return null;
    }


    @Override
    public List<TeacherDTO> getTeachers(StudentDTO student) {
        List<TeacherStudentEntity> teacherStudentList = teacherStudentRepository.findAllByStudent(student);
        List<TeacherDTO> teachers = new ArrayList<>();
        for (TeacherStudentEntity teacherStudent : teacherStudentList) {
            //teachers.add(teacherStudent.getTeacher());
        }
        return teachers;
    }

    @Override
    public boolean delete(TeacherDTO teacher, StudentDTO student) {
        TeacherStudentEntity teacherStudent = teacherStudentRepository.findByTeacherAndStudent(teacher, student);
        if (teacherStudent != null) {
            teacherStudentRepository.delete(teacherStudent);
            return true;
        }
        return false;
    }
}

