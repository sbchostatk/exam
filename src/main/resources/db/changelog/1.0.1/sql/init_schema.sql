create table Teacher (
                         teacher_id int primary key not null unique,
                         name varchar(30) not null,
                         middle_name varchar(30) not null,
                         last_name varchar(30) not null,
                         faculty varchar(30)
);


create table Student (
                         student_id int primary key not null unique,
                         name varchar(30) not null,
                         middle_name varchar(30) not null,
                         last_name varchar(30) not null,
                         specialty varchar(30),
                         course int not null
);

create table Teacher_Student (
                                 id int primary key not null unique,
                                 teacher_id int references Teacher(teacher_id) not null,
                                 student_id int references Student(student_id) not null
);

CREATE SEQUENCE student_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE teacher_id_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE teacher_student_id_seq START WITH 1 INCREMENT BY 1;
